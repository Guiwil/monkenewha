import Vue from 'vue'
import VueCompositionAPI, { createApp, h } from '@vue/composition-api'
import { createPinia, PiniaVuePlugin } from 'pinia'
import vuetify from '@/plugins/vuetify'
import App from './App.vue';
import router from './router'
import VueGlide from 'vue-glide-js'
import 'vue-glide-js/dist/vue-glide.css'

Vue.use(VueCompositionAPI)
Vue.use(VueGlide)



const app = createApp({
  router,
  pinia: createPinia(),
  vuetify: vuetify,
  render: () => h(App)
})
app.use(PiniaVuePlugin)

app.mount('#app')
